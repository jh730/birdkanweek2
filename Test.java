public class Test {
    public static void main(String[] args) {
        while (true) {
            int i = 0;
            ChessBoard p = new ChessBoard();
            p.player.chooseChess();
            p.printBoard();
            while(!p.winCondition()) {
                if (p.getNumber() % 2 == 0) {
                    boolean judge = p.putChess("X");
                    if (!judge) continue; // 如果位置不合法，则重新下
                }
                else {
                    boolean judge = p.putChess("O");
                    if (!judge) continue; // 如果位置不合法，则重新下
                }
                i++; // 棋子数加一
                p.setNumber(i);  // 设置棋子数
                p.printBoard();
                if(p.getNumber() == 9) {
                    System.out.println("This is a draw!");
                    break;
                }
            }
            if (!p.player.reStart()) {
                System.out.println("Game Over!");
                break;
            }
        }
    }
}
