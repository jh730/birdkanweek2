public class ChessBoard {
    private int number;
    Person player = new Person(); // 创建棋手
    String[][] board = new String[3][3]; // 创建棋盘
    // 设置棋子个数
    public void setNumber(int number) { 
        this.number = number;
    }
    // 获得棋子数
    public int getNumber() {
        return this.number;
    }
    // print a chessboard
    public void printBoard() {
        for (int i = 0; i < 3; i++) {
            System.out.println("-------------");
            for (int j = 0; j < 3; j++) {
                if (board[i][j] == null)
                    System.out.printf("|   ");
                else
                    System.out.printf("| %s ", board[i][j]);
            }
            System.out.println("|");
        }
        System.out.println("-------------");
    }
    // 判断位置是否合法
    public boolean judgement(int row, int column) {
        if (board[row][column] == null)  //该位置无棋子
            return true;
        else if (row > 2 || row < 0 || column > 2 || column < 0)  //越界
            return false;
        else   //该位置有棋子
            return false;
    }
    // 放置棋子
    public boolean putChess(String chess) {
        player.chessPlace(chess);
    // 若棋子位置合法，则存入数组
        if (judgement(player.row, player.column)) {
            board[player.row][player.column] = player.chesspiece;
            return true;
        }
        else {
            System.out.println("This site has been taken up, please choose another!");
            return false;
        }
    }
    // win condition
    public boolean winCondition() {
        int i, j;
        // row
        for (i = 0; i < 3; i++) {
            if (board[i][0] == board[i][1] && board[i][1] == board[i][2] && board[i][0] != null) {
                System.out.printf("%s player won!\n", board[i][0]);
                return true;
            }
        }
        // column
        for (j = 0; j < 3; j++) {
            if (board[0][j] == board[1][j] && board[1][j] == board[2][j] && board[0][j] != null) {
                System.out.printf("%s player won!\n", board[0][j]);
                return true;
            }
        }
        // diagonal
        if (board[0][0] == board[1][1] && board[1][1] == board[2][2] && board[1][1] != null) {
            System.out.printf("%s player won!\n", board[0][0]);
            return true;
        }
        if (board[0][2] == board[1][1] && board[1][1] == board[2][0] && board[1][1] != null) {
            System.out.printf("%s player won!\n", board[0][2]);
            return true;
        }
        return false;
    }
}