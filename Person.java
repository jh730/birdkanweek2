import java.util.Scanner;
public class Person {
    Scanner s = new Scanner(System.in);
    int row, column;
    String chesspiece; // 棋子类型
    // 选择棋子
    public void chooseChess() {
        String playerone, playertwo;
        do {
            System.out.printf("The first player chooses chess(X or O)：");
            playerone = s.nextLine();
        }while(!(playerone.equals("X") || playerone.equals("x") || playerone.equals("O") || playerone.equals("o")));
        if (playerone.equals("X") || playerone.equals("x")){
            playertwo = "O";
            System.out.printf("The first player is %s, the second player is %s\n", playerone,playertwo);
        }
        else {
            playertwo = "X";
            System.out.printf("The first player is %s, the second player is %s\n", playerone,playertwo);
        }
    }
    // 选择棋子的位置
    public void chessPlace(String chesspiece) {
        do {
            System.out.printf("Enter a row (1, 2 or 3) for player %s:", chesspiece);
            row = s.nextInt() - 1;
            s.nextLine();
        }while(row < 0 || row > 2);
        do {
            System.out.printf("Enter a column (1, 2 or 3) for player %s:", chesspiece);
            column = s.nextInt() - 1;
            s.nextLine();
        }while(column < 0 || column > 2);
        this.chesspiece = chesspiece;
    }
    // 选择是否开始下一局
    public boolean reStart() {
        Scanner s = new Scanner(System.in);
        String flag;
        System.out.printf("Do you want a new game?(Y or N):");
        flag = s.nextLine();
        s.close();
        if (flag.equals("Y") || flag.equals("y"))
            return true;
        else
            return false;
    }
}